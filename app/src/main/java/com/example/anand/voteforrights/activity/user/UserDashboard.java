package com.example.anand.voteforrights.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.activity.user.adapter.UserPartyAdapter;
import com.example.anand.voteforrights.entity.Party;
import com.example.anand.voteforrights.entity.User;
import com.example.anand.voteforrights.firebase.FirebaseManager;
import com.example.anand.voteforrights.utility.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anand on 11/03/18 at 2:03 AM.
 */

public class UserDashboard extends AppCompatActivity {

    private static final String TAG = "UserDashboard";
    private static final String KEY_NUMBER = "NUMBER";

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.already_voted)
    AppCompatTextView mAlreadyVotedMessage;

    @BindView(R.id.voted_image)
    AppCompatImageView mAlreadyVotedImage;

    String number;
    User currentUser;

    public static Intent getIntent(Context context, String number) {
        Intent intent = new Intent(context, UserDashboard.class);
        intent.putExtra(KEY_NUMBER, number);

        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dashboard);
        ButterKnife.bind(this);

        number = getIntent().getStringExtra(KEY_NUMBER);

        final UserPartyAdapter mAdapter = new UserPartyAdapter(
                new ArrayList<>(Collections.singletonList(new Party("ANAND", "DEMO"))),
                new UserPartyAdapter.OnPartyVotedListener() {
                    @Override
                    public void onPartyVoted(final Party party) {
                        Utility.loading(true, UserDashboard.this);
                        FirebaseManager.getInstance().voteForParty(party,
                                new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError,
                                                           DatabaseReference databaseReference) {
                                        Utility.loading(false, UserDashboard.this);
                                        if (databaseError == null) {
                                            mRecyclerView.setVisibility(View.GONE);
                                            mAlreadyVotedMessage.setVisibility(View.VISIBLE);
                                            mAlreadyVotedImage.setVisibility(View.VISIBLE);

                                            FirebaseManager.getInstance().updateVotedUser(number, party,
                                                    new DatabaseReference.CompletionListener() {
                                                        @Override
                                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                            mAlreadyVotedMessage.setText(
                                                                    "You have already voted for " + party.getName() + "\nNow sit back and relax for the results");
                                                            Toast.makeText(UserDashboard.this, "Party Voted Successfuly", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                            Log.e(TAG, "onComplete: ");
                                        }
                                    }
                                });
                    }
                });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        FirebaseManager.getInstance().getUser(number, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    currentUser = dataSnapshot.getValue(User.class);
                    if (currentUser != null && currentUser.isVoted()) {
                        mRecyclerView.setVisibility(View.GONE);
                        mAlreadyVotedMessage.setText("You have already voted for " + currentUser.getPartyVoted() + "\nNow sit back and relax for the results");
                        mAlreadyVotedMessage.setVisibility(View.VISIBLE);
                        mAlreadyVotedImage.setVisibility(View.VISIBLE);
                    } else {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAlreadyVotedMessage.setVisibility(View.GONE);
                        mAlreadyVotedImage.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseManager.getInstance().getParties(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Party> parties = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    parties.add(snapshot.getValue(Party.class));
                }
                mAdapter.swapAll(parties);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
