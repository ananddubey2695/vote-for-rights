package com.example.anand.voteforrights.activity.admin.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.entity.Party;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anand on 17/03/18.
 */

public class PartyAdapter extends RecyclerView.Adapter<PartyAdapter.PartyViewHolder> {

    private ArrayList<Party> parties;

    public PartyAdapter(ArrayList<Party> parties) {
        this.parties = parties;
    }

    @Override
    public PartyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_party_admin, parent, false);

        return new PartyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PartyViewHolder holder, int position) {
        holder.mId.setText(parties.get(position).getId());
        holder.mName.setText(parties.get(position).getName());
        holder.mVotes.setText(parties.get(position).getVotes().toString());
    }

    @Override
    public int getItemCount() {
        return parties.size();
    }

    public void swapAll(ArrayList<Party> parties) {
        if (this.parties != null && !this.parties.isEmpty()) {
            this.parties.clear();
            this.parties.addAll(parties);
        } else this.parties = parties;
        notifyDataSetChanged();
    }

    class PartyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.id)
        AppCompatTextView mId;

        @BindView(R.id.name)
        AppCompatTextView mName;

        @BindView(R.id.votes)
        AppCompatTextView mVotes;

        public PartyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
