package com.example.anand.voteforrights.activity.admin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.widget.Toast;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.entity.Party;
import com.example.anand.voteforrights.firebase.FirebaseManager;
import com.example.anand.voteforrights.utility.Utility;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anand on 17/03/18.
 */

public class AddParty extends AppCompatActivity {

    private static final String TAG = "AddParty";

    @BindView(R.id.name)
    AppCompatEditText mName;

    @BindView(R.id.slogan)
    AppCompatEditText mSlogan;

    @OnClick(R.id.add_party)
    void onAdd() {
        Utility.loading(true, this);
        FirebaseManager.getInstance().addParty(
                new Party(mName.getText().toString(), mSlogan.getText().toString()),
                new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Utility.loading(false, AddParty.this);
                        if (databaseError == null) {
                            Toast.makeText(AddParty.this, "Party has been added successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(AddParty.this, "Unable to add party\nPlease try again later", Toast.LENGTH_SHORT).show();
                        }
                        Log.e(TAG, "onComplete: " + databaseError);
                    }
                });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_party);
        ButterKnife.bind(this);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, AddParty.class);
    }
}
