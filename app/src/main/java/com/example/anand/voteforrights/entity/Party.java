package com.example.anand.voteforrights.entity;

import java.util.Random;

/**
 * Created by anand on 17/03/18.
 */

public class Party {

    private String id;

    private String name;

    private String slogan;

    private Long votes = 0L;

    public Party(String name, String slogan) {
        Random random = new Random();
        this.name = name;
        this.slogan = slogan;
        this.id = String.valueOf(random.nextInt(1000));
    }

    public Party() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public Long getVotes() {
        return votes;
    }

    public void setVotes(Long votes) {
        this.votes = votes;
    }
}
