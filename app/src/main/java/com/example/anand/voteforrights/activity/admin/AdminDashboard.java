package com.example.anand.voteforrights.activity.admin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.activity.admin.adapter.PartyAdapter;
import com.example.anand.voteforrights.entity.Party;
import com.example.anand.voteforrights.firebase.FirebaseManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anand on 11/03/18 at 2:03 AM.
 */

public class AdminDashboard extends AppCompatActivity {

    private static final String TAG = "AdminDashboard";

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @OnClick(R.id.add_party)
    void onAdd() {
        startActivity(AddParty.getIntent(this));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
        ButterKnife.bind(this);

        final PartyAdapter mAdapter = new PartyAdapter(new ArrayList<Party>(Collections.singletonList(new Party("ANAND", "DEMO"))));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        FirebaseManager.getInstance().getParties(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "onDataChange: " + dataSnapshot.getValue());
                ArrayList<Party> parties = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    parties.add(snapshot.getValue(Party.class));
                }
                mAdapter.swapAll(parties);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ", databaseError.toException());
            }
        });
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, AdminDashboard.class);
    }
}
