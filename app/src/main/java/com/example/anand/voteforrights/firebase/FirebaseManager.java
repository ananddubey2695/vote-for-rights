package com.example.anand.voteforrights.firebase;

import com.example.anand.voteforrights.entity.Party;
import com.example.anand.voteforrights.entity.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by anand on 06/03/18.
 */

public class FirebaseManager {

    private static FirebaseManager instance;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mAuth;
    private DatabaseReference userTable;
    private DatabaseReference partyTable;

    private FirebaseManager() {
        mDatabase = FirebaseDatabase.getInstance();
        mAuth = mDatabase.getReference().child("auth");
        userTable = mDatabase.getReference().child("users");
        partyTable = mDatabase.getReference().child("parties");
    }

    public static FirebaseManager getInstance() {
        if (instance == null)
            instance = new FirebaseManager();
        return instance;
    }

    // TODO: 11/03/18 handle already user exists
    public void addUser(User user, CompletionListener listener) {
        mAuth.child(user.getPhone()).setValue(user.getPassword(), listener);

        userTable.child(user.getPhone()).setValue(user);
    }

    public void loginUser(ValueEventListener listener) {
        mAuth.addListenerForSingleValueEvent(listener);
    }

    public void addParty(Party party, CompletionListener listener) {
        partyTable.child(party.getId()).setValue(party, listener);
    }

    public void getParties(ValueEventListener listener) {
        partyTable.addValueEventListener(listener);
    }

    public void getUser(String number, ValueEventListener listener) {
        userTable.child(number).addListenerForSingleValueEvent(listener);
    }

    public void voteForParty(Party party, CompletionListener listener) {
        partyTable.child(party.getId()).child("votes").setValue(party.getVotes() + 1, listener);
    }

    public void updateVotedUser(String number, Party party, CompletionListener listener) {
        userTable.child(number).child("voted").setValue(true, listener);
        userTable.child(number).child("party_voted").setValue(party.getName());
    }
}
