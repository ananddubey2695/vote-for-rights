package com.example.anand.voteforrights.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.widget.Toast;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.activity.admin.AdminDashboard;
import com.example.anand.voteforrights.activity.user.UserDashboard;
import com.example.anand.voteforrights.firebase.FirebaseManager;
import com.example.anand.voteforrights.utility.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anand on 11/03/18.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    @BindView(R.id.number)
    AppCompatEditText mPhoneNumber;

    @BindView(R.id.password)
    AppCompatEditText mPassword;

    @OnClick(R.id.register_button)
    void onRegister() {
        startActivity(RegistrationActivity.getIntent(this));
    }

    @OnClick(R.id.login_button)
    void onLogin() {
        Utility.loading(true, this);

        if (!mPhoneNumber.getText().toString().isEmpty()
                && mPhoneNumber.getText().toString().matches("^[789]\\d{9}$")
                && !mPassword.getText().toString().isEmpty()) {

            Log.e(TAG, "onLogin: ");

            FirebaseManager.getInstance().loginUser(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.e(TAG, "onDataChange: DATASNAP " + dataSnapshot);
                    if (dataSnapshot.child(mPhoneNumber.getText().toString()).exists()
                            && mPassword.getText().toString().equals(dataSnapshot
                            .child(mPhoneNumber.getText().toString()).getValue())) {

                        FirebaseManager.getInstance().getUser(mPhoneNumber.getText().toString(),
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Utility.loading(false, LoginActivity.this);
                                        if (dataSnapshot.child("is_admin").getValue(Boolean.class)) {
                                            startActivity(AdminDashboard.getIntent(LoginActivity.this));
                                            finish();
                                        } else {
                                            startActivity(UserDashboard.getIntent(LoginActivity.this,
                                                    mPhoneNumber.getText().toString()));
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                    } else {
                        Utility.loading(false, LoginActivity.this);
                        Toast.makeText(LoginActivity.this,
                                "Invalid Username and password", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            Utility.loading(false, this);
            Toast.makeText(this, "Please enter valid credentials", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }
}
