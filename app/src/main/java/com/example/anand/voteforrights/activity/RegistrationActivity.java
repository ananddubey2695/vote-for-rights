package com.example.anand.voteforrights.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.widget.Toast;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.entity.User;
import com.example.anand.voteforrights.firebase.FirebaseManager;
import com.example.anand.voteforrights.utility.Utility;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = "RegistrationActivity";

    @BindView(R.id.number)
    AppCompatEditText mPhoneNumber;

    @BindView(R.id.password)
    AppCompatEditText mPassword;

    @BindView(R.id.confirm_password)
    AppCompatEditText mConfirmPassword;

    @BindView(R.id.aadhar)
    AppCompatEditText mAadhar;

    @BindView(R.id.name)
    AppCompatEditText mName;

    @BindView(R.id.address)
    AppCompatEditText mAddress;

    @BindView(R.id.voter_id)
    AppCompatEditText mVotedId;

    @OnClick(R.id.register_button)
    void onClick() {

        if (!mPhoneNumber.getText().toString().isEmpty()
                && mPhoneNumber.getText().toString().matches("^[789]\\d{9}$")
                && !mPassword.getText().toString().isEmpty()
                && !mConfirmPassword.getText().toString().isEmpty()
                && !mAadhar.getText().toString().isEmpty()
                && mAadhar.getText().toString().length() == 12
                && !mName.getText().toString().isEmpty()
                && !mAddress.getText().toString().isEmpty()
                && !mVotedId.getText().toString().isEmpty()) {
            if (mPassword.getText().toString().equals(mConfirmPassword.getText().toString())) {

                Utility.loading(true, this);
                Log.e(TAG, "onClick: ");
                User user = new User();
                user.setPhone(mPhoneNumber.getText().toString());
                user.setPassword(mPassword.getText().toString());
                user.setAadharNumber(mAadhar.getText().toString());
                user.setName(mName.getText().toString());
                user.setAddress(mAddress.getText().toString());
                user.setVoterId(mVotedId.getText().toString());

                FirebaseManager.getInstance().addUser(user, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Utility.loading(false, RegistrationActivity.this);
                        if (databaseError != null) {
                            Toast.makeText(RegistrationActivity.this, "Unable to Register\nPlease try again later", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RegistrationActivity.this, "You have been successfully registered", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
            } else
                Toast.makeText(this, "Password and Confirm Password does'nt match", Toast.LENGTH_SHORT).show();

        } else
            Toast.makeText(this, "Please enter all valid information", Toast.LENGTH_SHORT).show();
    }

    // TODO: 11/03/18 Handle validations


    public static Intent getIntent(Context context) {
        return new Intent(context, RegistrationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
    }
}
