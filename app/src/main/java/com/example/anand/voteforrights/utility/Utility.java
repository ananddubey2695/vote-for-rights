package com.example.anand.voteforrights.utility;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by anand on 18/03/18 at 2:17 AM.
 */

public class Utility {
    private static ProgressDialog dialog;

    public static void loading(boolean visible, Context context) {
        if (visible) {
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading...\nPlease wait");
            dialog.show();
            dialog.setIndeterminate(true);
        } else if (dialog != null) {
            dialog.dismiss();
        }

    }
}
