package com.example.anand.voteforrights.entity;

import com.google.firebase.database.PropertyName;

/**
 * Created by anand on 11/03/18.
 */

public class User {
    private String name;
    private String surname;
    private String phone;
    private String address;
    private String partyVoted;
    private boolean admin = false;
    private String aadharNumber;
    private boolean isVoted = false;
    private String password;
    private String voterId;

    public User() {
    }

    public User(String name, String surname, String phone, String address, String partyVoted, boolean admin, String aadharNumber, boolean isVoted, String password, String votreId) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.address = address;
        this.partyVoted = partyVoted;
        this.admin = admin;
        this.aadharNumber = aadharNumber;
        this.isVoted = isVoted;
        this.password = password;
        this.voterId = votreId;
    }

    @PropertyName("is_admin")
    public boolean isAdmin() {
        return admin;
    }

    @PropertyName("is_admin")
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @PropertyName("party_voted")
    public String getPartyVoted() {
        return partyVoted;
    }

    @PropertyName("party_voted")
    public void setPartyVoted(String partyVoted) {
        this.partyVoted = partyVoted;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    @PropertyName("voted")
    public boolean isVoted() {
        return isVoted;
    }

    @PropertyName("voted")
    public void setVoted(boolean voted) {
        isVoted = voted;
    }

    @PropertyName("voter_id")
    public String getVoterId() {
        return voterId;
    }

    @PropertyName("voter_id")
    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }
}
