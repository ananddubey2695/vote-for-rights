package com.example.anand.voteforrights.activity.user.adapter;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anand.voteforrights.R;
import com.example.anand.voteforrights.entity.Party;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.RecyclerView.Adapter;
import static android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * Created by anand on 17/03/18 at 3:51 PM.
 */

public class UserPartyAdapter extends Adapter<UserPartyAdapter.UserPartyHolder> {

    private static final String TAG = "UserPartyAdapter";

    private ArrayList<Party> parties;

    private OnPartyVotedListener listener;

    public UserPartyAdapter(ArrayList<Party> parties, OnPartyVotedListener listener) {
        this.parties = parties;
        this.listener = listener;
    }

    @Override
    public UserPartyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_party_user, parent, false);

        return new UserPartyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserPartyHolder holder, int position) {
        final Party party = parties.get(position);
        holder.mId.setText(party.getId());
        holder.mName.setText(party.getName());
        holder.mSlogan.setText(party.getSlogan());

        holder.mVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPartyVoted(party);
                Log.e(TAG, "onClick: ");
            }
        });
    }

    public void swapAll(ArrayList<Party> parties) {
        if (this.parties != null && !this.parties.isEmpty()) {
            this.parties.clear();
            this.parties.addAll(parties);
        } else this.parties = parties;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return parties.size();
    }

    class UserPartyHolder extends ViewHolder {

        @BindView(R.id.id)
        AppCompatTextView mId;

        @BindView(R.id.name)
        AppCompatTextView mName;

        @BindView(R.id.slogan)
        AppCompatTextView mSlogan;

        @BindView(R.id.vote_button)
        AppCompatButton mVote;

        UserPartyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnPartyVotedListener {
        void onPartyVoted(Party party);
    }
}
